import React, { useState } from "react";
import TreeChart from "./TreeChart";
import "./App.css";

const initialData = {
  name: "Full Stack Development",
  children: [
    {
      name: "Front End",
      children: [
        {
          name: "React"
        },
        {
          name: "Angular"
        },
        {
          name: "Bootstrap"
        }
      ]
    },
    {
      name: "Back End",
      children: [
        {
          name: "Express"
        },
        {
          name: "Django"
        },
        {
          name: "Pyramid",
        }
      ]
    }
  ]
};

function App() {
  const [data, setData] = useState(initialData);

  return (
    <React.Fragment>
      <h1>Node Chart</h1>
      <TreeChart data={data} />
    </React.Fragment>
  );
}

export default App;